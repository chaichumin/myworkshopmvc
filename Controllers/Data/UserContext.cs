﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAppEF.Controllers.Data;
using WebAppEF.Models;

namespace WebAppEF.Controllers.Data
{
    public class UserContext : DbContext
    {
        public UserContext(DbContextOptions options)
           : base(options)
        {
        }

        public DbSet<UserModel> UserModel { get; set; }

    }
}
